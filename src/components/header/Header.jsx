import { useEffect, useState } from "react";
import "./header.scss";

export default function Header({ isBig }) {
  const linkArray = [
    { title: "Work", link: "#" },
    { title: "About", link: "#" },
  ];
  const [isActive, setIsActive] = useState("");

  useEffect(() => {
    setIsActive("Work");
  }, []);

  const onClickNavigateHeader = (item) => {
    setIsActive(item.title);
  };

  return (
    <div className='header'>
      {linkArray.map((item, index) => (
        <span
          key={index}
          className={`${isActive === item.title && "active"} `}
          onClick={() => onClickNavigateHeader(item)}
        >
          {item.title}
        </span>
      ))}
    </div>
  );
}
