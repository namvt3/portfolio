import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./bannerContainer.scss";
import defaultImage from "../../assets/Xina.png";
import ScrollDownPNG from "../../assets/scroll-down.png";

export default function BannerContainer({ src, text }) {
  const navigate = useNavigate();
  const [offsetY, setOffsetY] = useState(0);

  useEffect(() => {
    document.addEventListener("scroll", handleScroll);
    return () => {
      document.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const handleScroll = () => {
    setOffsetY(window.pageYOffset);
  };

  const onClickBack = () => {
    navigate("/");
  };

  const onClickScrollDown = () => {
    document.body.scrollTop = window.innerHeight;
    document.documentElement.scrollTop = window.innerHeight;
  };

  return (
    <div className='banner-container' style={{ height: `${window.innerHeight}px` }}>
      <img
        src={src}
        onError={(e) => e.target.setAttribute("src", `${defaultImage}`)}
        alt='banner'
        className='my-work-banner'
        style={{ transform: `translateY(${offsetY * 0.5}px)` }}
      />
      <span className='back' onClick={onClickBack}>
        Back
      </span>
      <div className='introdution'>
        <h1>
          <p>{text.title1}</p>
          <p>{text.title2}</p>
        </h1>
        <p>
          <b>{text.position}</b>
        </p>
        <span>{text.longText}</span>
        <p className='done-with'>
          <b>Done with</b>
          <br />
          <b>AKQA Amsterdam</b>
        </p>
        <p className='scroll-down' onClick={onClickScrollDown}>
          Scroll down
        </p>
        <img
          src={ScrollDownPNG}
          alt=''
          className='scroll-down-image'
          onClick={onClickScrollDown}
        />
      </div>
    </div>
  );
}
