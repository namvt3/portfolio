import { useState, useEffect, useRef } from "react";
import "./homeSliderImage.scss";
import defaultImage from "../../assets/Xina.png";
import { imageSourceArray } from "../../constants/constants";
import { useDispatch } from "react-redux";
import { setCurrentShowingWork } from "../../reducers/backgroundImage";

export default function HomeSliderImage({ isBig, setIsBig }) {
  const windowWidth = window.innerWidth;
  const windowHeight = window.innerHeight;

  const [isMouseDown, setIsMouseDown] = useState(false);
  const [progressTrack, setProgressTrack] = useState(window.innerWidth / 2 - 298 / 2);
  const [progressImage, setProgressImage] = useState(100);
  const [firstMousePosition, setFirstMousePosition] = useState(null);
  const [shouldExecuteClick, setShouldExecuteClick] = useState(true);
  const [hasTransition, setHasTransition] = useState(true);
  const longClickTimeoutRef = useRef();
  const dispatch = useDispatch();

  useEffect(() => {
    document.addEventListener("mousemove", handleOnMouseMove);
    document.addEventListener("mouseup", handleOnMouseUp);

    return () => {
      document.removeEventListener("mousemove", handleOnMouseMove);
      document.removeEventListener("mouseup", handleOnMouseUp);
    };
    // eslint-disable-next-line
  }, [isMouseDown]);

  const handleOnMouseMove = (e) => {
    if (isMouseDown) {
      const mouseDelta = parseFloat(e.clientX - firstMousePosition),
        percentage = (mouseDelta / (1570 - 298)) * 100,
        newPercentage = progressImage + percentage;
      if (newPercentage >= 0 && newPercentage <= 100) {
        setProgressTrack(progressTrack + mouseDelta);
        setProgressImage(newPercentage);
      }
    }
  };

  const handleOnMouseUp = (e) => {
    setIsMouseDown(false);
    setFirstMousePosition(e.clientX);

    clearTimeout(longClickTimeoutRef.current);
    setShouldExecuteClick(true);
  };

  const onMouseDownSlider = (e) => {
    setIsMouseDown(true);
    setTimeout(() => setHasTransition(false), 550);
    setFirstMousePosition(e.clientX);
    longClickTimeoutRef.current = setTimeout(() => setShouldExecuteClick(false), 100);
  };

  const onMouseUpImage = (item) => {
    clearTimeout(longClickTimeoutRef.current);
    if (shouldExecuteClick) {
      setHasTransition(true);
      setIsBig((prev) => !prev);
      if (isBig) {
        dispatch(setCurrentShowingWork(item));
      }
    }
    setShouldExecuteClick(true);
  };

  return (
    <div
      id='image-track'
      className={isBig ? "big" : "small"}
      style={{
        left: `${isBig ? progressTrack : windowWidth - 430}px`,
        top: `${isBig ? windowHeight / 2 - 209 : windowHeight - 80}px`,
        transition: `${hasTransition ? "0.5s ease" : "none"}`,
      }}
      onMouseDown={onMouseDownSlider}
      onMouseUp={() => {
        if (!isBig) {
          setHasTransition(true);
          setIsBig(true);
        }
      }}
    >
      {imageSourceArray.map((item, index) => (
        <img
          key={index}
          draggable='false'
          src={item.src}
          onError={(e) => e.target.setAttribute("src", `${defaultImage}`)}
          alt=''
          className='image'
          style={{ objectPosition: `${progressImage}% 50%` }}
          onMouseUp={() => onMouseUpImage(item)}
        />
      ))}
    </div>
  );
}
