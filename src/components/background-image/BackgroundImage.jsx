import defaultImage from "../../assets/Xina.png";
import "./backgroundImage.scss";
import { useSelector } from "react-redux";

const BackgroundImage = ({ isBig, onClickIntro }) => {
  const { currentShowingWork } = useSelector((state) => state.backgroundImage);

  return (
    <>
      <div
        id='background-image'
        style={{
          backgroundImage: `url(${currentShowingWork.src}), url(${defaultImage})`,
        }}
      ></div>
      <div className={`my-work-intro ${isBig && "hide"}`} onClick={onClickIntro}>
        {currentShowingWork.intro}
      </div>
    </>
  );
};

export default BackgroundImage;
