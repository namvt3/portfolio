export const imageSourceArray = [
  {
    src: "https://wallpapers.com/images/featured/cat-and-dog-9tqegrvy3uazti1s.webp",
    intro: "Cute Dogs & Cats",
    path: "dogs-and-cats",
    banner: "https://images3.alphacoders.com/270/270209.jpg",
  },
  {
    src: "https://wallpapers.com/images/featured/2560x1440-car-klkb61fqls2epvei.webp",
    intro: "Awesome Supercars",
    path: "cars",
    banner: "https://images5.alphacoders.com/118/1188379.jpg",
  },
  {
    src: "https://wallpapers.com/images/featured/4k-space-9w27dqmc4nrs3xwd.jpg",
    intro: "Mysterious Space",
    path: "space",
    banner: "https://wallpapers.com/images/featured/outer-space-3pae4flbryaputyl.jpg",
  },
  {
    src: "https://wallpapers.com/images/featured/beauty-and-the-beast-taxeaex0hpp948ib.jpg",
    intro: "Dreamy Disney",
    path: "disney",
    banner:
      "https://wallpapers.com/images/featured/1920x1080-disney-saw6ntkbwb771to4.webp",
  },
  {
    src: "https://wallpapers.com/images/featured/best-friend-3nirgjlc34qxf68b.jpg",
    intro: "Travel Everywhere",
    path: "travel",
    banner: "https://wallpapers.com/images/featured/australia-5pzma85n2bzh94eh.jpg",
  },
];
