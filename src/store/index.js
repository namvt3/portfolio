import { configureStore } from "@reduxjs/toolkit";
import backgroundImage from "../reducers/backgroundImage";

export const store = configureStore({
  reducer: {
    backgroundImage,
  },
});
