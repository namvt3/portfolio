import { createSlice } from "@reduxjs/toolkit";
import { imageSourceArray } from "../constants/constants";

const initialState = {
  currentShowingWork: imageSourceArray[0],
};

export const backgroundImage = createSlice({
  name: "backgroundImage",
  initialState,
  reducers: {
    setCurrentShowingWork: (state, action) => {
      state.currentShowingWork = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setCurrentShowingWork } = backgroundImage.actions;

export default backgroundImage.reducer;
