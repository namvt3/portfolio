import "./dogsAndCats.scss";
import BannerContainer from "../../components/banner-container/BannerContainer";
import { imageSourceArray } from "../../constants/constants";
import { useEffect, useRef, useState } from "react";

export default function DogsAndCat() {
  const initialImages = useRef([
    {
      src: "https://wallpapers.com/images/featured/aesthetic-dogs-uspq31mus2iht0i9.webp",
      show: "hide",
    },
    {
      src: "https://wallpapers.com/images/featured/beautiful-cats-y4upwj5zz45novpx.webp",
      show: "hide",
    },
    {
      src: "https://wallpapers.com/images/high/sleeping-dog-and-cat-vrteu2xrb5aqwypg.jpg",
      show: "hide",
    },
    {
      src: "https://wallpapers.com/images/high/high-resolution-desktop-dog-and-cat-ll9pfuvh2yasdp6p.jpg",
      show: "hide",
    },
    {
      src: "https://wallpapers.com/images/high/studio-shot-cat-and-dog-cc7iwvbxdutm03ga.jpg",
      show: "hide",
    },
  ]);
  const imgElmRef = useRef(null);
  const scaleFlag = useRef(0.7);
  const endOfWorkRef = useRef(null);

  const [images, setImages] = useState(initialImages.current);
  const [nextWidth, setNextWidth] = useState(298);
  const [nextHeight, setNextHeight] = useState(418);
  const [scale, setScale] = useState(0.7);

  useEffect(() => {
    document.addEventListener("scroll", handleShowImages);
    document.addEventListener("wheel", handleResizeNextProjectImg);
    return () => {
      document.removeEventListener("scroll", handleShowImages);
      document.removeEventListener("wheel", handleResizeNextProjectImg);
    };
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (scaleFlag.current > 0.7) {
      document.body.style.overflow = "hidden";
    } else {
      setTimeout(() => (document.body.style.overflow = "inherit"), 200);
    }
    // eslint-disable-next-line
  }, [scaleFlag.current]);

  const handleShowImages = (e) => {
    const imageElementArray = document.getElementsByClassName(
      "content__image-container__image"
    );
    const cloneArr = [...images];
    for (let i = 0; i < imageElementArray.length; i++) {
      const distanceToTop = imageElementArray[i].getBoundingClientRect().top;
      if (distanceToTop / window.innerHeight < 0.7) {
        initialImages.current[i].show = "show";
        setImages(cloneArr);
      }
    }
  };

  const deltaWidth = window.innerWidth - 298;
  const deltaHeight = window.innerHeight - 418;

  const handleResizeNextProjectImg = (e) => {
    // Chỉ kích hoạt khi lăn đến element
    if (endOfWorkRef.current && endOfWorkRef.current.getBoundingClientRect().top < 0.2) {
      if (e.deltaY > 0) {
        if (scaleFlag.current < 1 - 0.3 / 7) {
          setNextWidth((prev) => prev + deltaWidth / 7);
          setNextHeight((prev) => prev + deltaHeight / 7);
          setScale((prev) => prev + 0.3 / 7);
          scaleFlag.current = scaleFlag.current + 0.3 / 7;
          // Nếu max size thì chuyển trang
        }
      } else {
        if (scaleFlag.current > 0.7) {
          setNextWidth((prev) => prev - deltaWidth / 7);
          setNextHeight((prev) => prev - deltaHeight / 7);
          setScale((prev) => prev - 0.3 / 7);
          scaleFlag.current = scaleFlag.current - 0.3 / 7;
        }
      }
    }
  };

  return (
    <div id='dogs-and-cats'>
      <BannerContainer
        src={imageSourceArray[0].banner}
        text={{
          title1: "Dogs and Cats",
          title2: "Are so Cute!",
          position: "Lead Designer",
          longText: `Who doesn't love dogs and cats? We all want to own some of them. Let's look at
        these cute photos below to make your day better.`,
        }}
      />

      <div className='content'>
        {images.map((item, index) => (
          <div key={index} className='content__image-container'>
            <img
              ref={imgElmRef}
              src={item.src}
              className={`content__image-container__image ${item.show}`}
              alt=''
            />
          </div>
        ))}
      </div>

      <div
        ref={endOfWorkRef}
        className='end-of-work'
        style={{ height: `${window.innerHeight}px` }}
      >
        <p className='end-of-work__next-project-text'>
          <span className='intro'>{imageSourceArray[1].intro}</span>
          <br />
          <span className='sub'>Next Project</span>
        </p>

        <div
          className='end-of-work__next-project-container'
          style={{
            width: `${nextWidth}px`,
            height: `${nextHeight}px`,
          }}
        >
          <img
            className='end-of-work__next-project-image'
            alt=''
            src={imageSourceArray[1].src}
            style={{
              width: `${window.innerWidth}px`,
              height: `${window.innerHeight}px`,
              transform: `scale(${scale})`,
            }}
          />
        </div>
      </div>
    </div>
  );
}
