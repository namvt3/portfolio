import "./home.scss";
import HomeSliderImage from "../../components/home-slider-images/HomeSliderImage";
import Header from "../../components/header/Header";
import { useEffect, useState } from "react";
import BackgroundImage from "../../components/background-image/BackgroundImage";
import defaultImage from "../../assets/Xina.png";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { imageSourceArray } from "../../constants/constants";

export default function Home() {
  const { currentShowingWork } = useSelector((state) => state.backgroundImage);
  const navigate = useNavigate();

  const [isBig, setIsBig] = useState(false);
  const [changePage, setChangePage] = useState(false);
  const [nextPageImg, setNextPageImg] = useState(imageSourceArray[0].banner);

  useEffect(() => {
    const index = imageSourceArray.indexOf(currentShowingWork);
    setNextPageImg(imageSourceArray[index].banner);
  }, [currentShowingWork]);

  const onClickIntro = () => {
    setChangePage(true);
    setTimeout(() => navigate(`/${currentShowingWork.path}`), 750);
  };

  return (
    <div id='home' className={changePage ? "go-up" : "normal"}>
      <BackgroundImage isBig={isBig} onClickIntro={onClickIntro} />
      <Header isBig={isBig} />
      <HomeSliderImage isBig={isBig} setIsBig={setIsBig} />
      <img
        id='next-page-banner'
        className={changePage ? "go-up" : "normal"}
        alt=''
        src={nextPageImg}
        onError={(e) => e.target.setAttribute("src", `${defaultImage}`)}
        style={{ height: `${window.innerHeight}px` }}
      />
    </div>
  );
}
