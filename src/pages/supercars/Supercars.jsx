// import "./supercars.scss";
import BannerContainer from "../../components/banner-container/BannerContainer";
import { imageSourceArray } from "../../constants/constants";

export default function Supercars() {
  return (
    <div id='dogs-and-cats'>
      <BannerContainer
        src={imageSourceArray[1].banner}
        text={{
          title1: "Fast &",
          title2: "Furious",
          position: "Lead Designer",
          longText: `We make cars that elegant, eco friendly, strong and full of careness for your safety. Wanna see these babies?`,
        }}
      />
    </div>
  );
}
