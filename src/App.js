import { Route, Routes } from "react-router-dom";
import { imageSourceArray } from "./constants/constants";
import Home from "./pages/home/Home";
import DogsAndCat from "./pages/dogs-and-cats/DogsAndCats";
import Supercars from "./pages/supercars/Supercars";

function App() {
  return (
    <Routes>
      <Route path='/' element={<Home />} />
      <Route path={`${imageSourceArray[0].path}`} element={<DogsAndCat />} />
      <Route path={`${imageSourceArray[1].path}`} element={<Supercars />} />
    </Routes>
  );
}

export default App;
